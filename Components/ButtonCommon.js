import { View, Text, StyleSheet } from 'react-native';
import React from 'react';
import { DIMENSIONS } from '../Styles/Dimensions';

export default function ButtonCommon({ btnName }) {
  return (
    <View style={styles.buttonSI}>
      <Text style={styles.buttonTextSI}>{btnName}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonSI: {
    backgroundColor: '#2ad983',
    width: DIMENSIONS.screenWidth / 1.2,
    alignItems: 'center',
    marginTop: DIMENSIONS.screenWidth / 20,
    padding: DIMENSIONS.screenWidth / 25,
    borderRadius: 20,
  },
  buttonTextSI: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'white',
  },
});
