import {View, StyleSheet, TextInput} from 'react-native';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { DIMENSIONS } from '../Styles/Dimensions';

export default function InputCommon({iconName, iconColor, iconSize, placeholder, ...rest}) {
  return (
    <View style={styles.inputSubContainerSI}>
      <Ionicons
        style={styles.inputIconSI}
        size={iconSize}
        name={iconName}
        color={iconColor}
      />
      <TextInput style={styles.inputSI} placeholder={placeholder} {...rest}/>
    </View>
  );
}

const styles = StyleSheet.create({
  inputSubContainerSI: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: DIMENSIONS.screenWidth / 1.2,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 20,
    marginTop: DIMENSIONS.screenWidth / 20,
  },
  inputIconSI: {
    marginLeft: DIMENSIONS.screenWidth / 20,
    marginRight: DIMENSIONS.screenWidth / 40,
  },
  inputSI: {
    flex: 2.5/3
  }
});
