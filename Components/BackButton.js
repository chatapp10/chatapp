import { View, Text, StyleSheet } from 'react-native';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { DIMENSIONS } from '../Styles/Dimensions';

export default function BackButton() {
  return (
    <View>
      <Ionicons
        style={styles.backButton}
        name="chevron-back-outline"
        color="black"
        size={22}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  backButton: {
    marginLeft: DIMENSIONS.screenWidth / 20,
    marginTop: DIMENSIONS.screenWidth / 20,
  },
});
