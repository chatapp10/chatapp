import { View, Text, StyleSheet, Image } from 'react-native';
import React from 'react';
import { DIMENSIONS } from '../Styles/Dimensions';

export default function LogoCommon() {
  return (
    <View style={styles.logoSIContainer}>
      <Image
        style={styles.logoSI}
        source={require('../Images/WaitingScreenLogo.png')}
      />
      <Text style={styles.titleSI}>WhatsApp</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  logoSIContainer: {
    alignItems: 'center',
    marginTop: DIMENSIONS.screenWidth / 20,
  },
  logoSI: {
    width: DIMENSIONS.screenWidth / 3,
    height: DIMENSIONS.screenWidth / 3,
  },
  titleSI: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'black',
  },
});
