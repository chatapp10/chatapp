import {
    View,
    Text,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
  } from 'react-native';
  import React, {useState, useEffect} from 'react';
  import Ionicons from 'react-native-vector-icons/Ionicons';
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const itemWidth = windowWidth - 60;

export default function CallWait({navigation, route}) {
    const dataItem = route.params.dataItem
  return (
    <View>
<View style={{flexDirection:'column',width:windowWidth,height:600,backgroundColor:'#06a88e',justifyContent:'center',alignItems:'center'}}>
<Image
            style={{
              width: 150,
              height: 150,
              resizeMode: 'cover',
              borderRadius: 75,
            }}
            source={{
                uri: route?.params.dataItem.url,
            }}></Image>
            <Text style={{fontSize:20,fontWeight:'bold',marginTop:20}}> {  route?.params.dataItem.name}</Text>
            <Text style={{fontSize:20}}> Calling...</Text>
</View>
      <View style={{flexDirection:'row',justifyContent:'space-around',width:windowWidth,height:70,marginTop:30}}>
        <TouchableOpacity onPress={() => navigation.goBack()} style={{width:70,height:70,borderRadius:35,backgroundColor:'grey',alignItems:'center'}}><Ionicons style={{lineHeight:70}}
              name="ios-call"
              size={30}
              color={'red'}></Ionicons></TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('ProfileCall', {dataItem})} style={{width:70,height:70,borderRadius:35,backgroundColor:'grey',alignItems:'center'}}><Ionicons style={{lineHeight:70}}
              name="ios-call"
              size={30}
              color={'#06a88e'}></Ionicons></TouchableOpacity>
      </View>
    </View>
  )
}