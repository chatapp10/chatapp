import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import React, {useState, useEffect} from 'react';
import {onValue} from 'firebase/database';
import {firebaseDatabase, firebaseDatabaseRef} from '../firebase/firebase';
import {async} from '@firebase/util';
import AsyncStorage from '@react-native-async-storage/async-storage';

const windowWidth = Dimensions.get('window').width;
const windowHieght = Dimensions.get('window').height;
const itemWidth = windowWidth - 60;

export default function Chatmenu({navigation}) {
  const [users, setUsers] = useState([]);
  const [loggedUser, setLogged] = useState('');
  const renderItem = ({item, index}) => {
    return <ChatItem dataItem={item} navigation={navigation} />;
  };
  const [chatHistory, setChatHistory] = useState([]);
  const menuCommon = [
    {
      title: 'Your Status',
      uri: 'https://randomuser.me/api/portraits/women/19.jpg',
    },
    {title: 'Emanz', uri: 'https://randomuser.me/api/portraits/women/20.jpg'},
    {
      title: 'Eccentric',
      uri: 'https://randomuser.me/api/portraits/women/21.jpg',
    },
    {title: 'Symonds', uri: 'https://randomuser.me/api/portraits/women/22.jpg'},
    {title: 'Symonds', uri: 'https://randomuser.me/api/portraits/women/23.jpg'},
    {title: 'Symonds', uri: 'https://randomuser.me/api/portraits/women/24.jpg'},
  ];
  function ChatItem({dataItem}) {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('Message', {dataItem, loggedUser})}
        style={{
          height: 100,
          paddingTop: 20,
          paddingStart: 10,
          flexDirection: 'row',
        }}>
        <View>
          <Image
            style={{
              width: 70,
              height: 70,
              resizeMode: 'cover',
              borderRadius: 35,
              marginRight: 15,
              marginStart: 10,
            }}
            source={{
              uri: dataItem.url,
            }}
          />
          {dataItem.numberOfUnreadMessages > 0 && (
            <Text
              style={{
                backgroundColor: 'red',
                position: 'absolute',
                right: 7,
                fontSize: 16 * 0.8,
                borderRadius: 10,
                paddingHorizontal: dataItem.numberOfUnreadMessages > 9 ? 2 : 4,
                color: 'white',
              }}>
              {dataItem.numberOfUnreadMessages}
            </Text>
          )}
        </View>
        <View
          style={{
            flexDirection: 'column',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: windowWidth - 100,
            }}>
            <Text
              style={{
                color: 'black',
                fontSize: 15,
                fontWeight: '600',
                marginLeft: 10,
              }}>
              {dataItem.name}
            </Text>
            <Text
              style={{
                color: 'gray',
                fontSize: 15 * 0.8,
                marginRight: 10,
              }}>
              {dataItem.timepass}
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            {dataItem.isRead == true ? (
              <Ionicons
                style={{marginRight: 10, alignSelf: 'center'}}
                name="checkmark-done-outline"
                size={14}
                color={'#06a88e'}></Ionicons>
            ) : (
              <Ionicons
                style={{marginRight: 10, alignSelf: 'center'}}
                name="checkmark-sharp"
                size={14}
                color={'gray'}></Ionicons>
            )}
            <Text
              style={{
                color: 'black',
                fontSize: 16,
                color: 'gray',
              }}>
              {dataItem.firstmessage}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'column',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-end',
          }}></View>
      </TouchableOpacity>
    );
  }

  const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#CED0CE',
        }}
      />
    );
  };
  useEffect(() => {
    onValue(firebaseDatabaseRef(firebaseDatabase, 'users'), async snapshot => {
      if (snapshot.exists()) {
        let snapshotObject = snapshot.val();
        let stringUser = await AsyncStorage.getItem('loggedUser');
        setLogged(JSON.parse(stringUser));
        console.log(stringUser);
        console.log(snapshot.val());
        console.log(Object.keys(snapshotObject));
        setChatHistory(
          Object.keys(snapshotObject)
            .filter(item => item !== JSON.parse(stringUser))
            .map((eachKey, index) => {
              let eachObject = snapshotObject[eachKey];
              return {
                url: `https://randomuser.me/api/portraits/women/${index}.jpg`,
                uid: eachKey,
                name: eachObject.userName,
                firstmessage: 'Hello,Wich',
                numberOfUnreadMessages: 12,
                timepass: '03:00 PM',
                isRead: true,
              };
            }),
        );
      }
    });
  }, []);
  return (
    <>
      <View>
        <FlatList
          horizontal={true}
          keyExtractor={(item, index) => index.toString()}
          data={menuCommon}
          renderItem={({item, index}) => {
            return (
              <View style={{height: 150}}>
                <TouchableOpacity style={{alignItems: 'center'}}>
                  <Image
                    style={{
                      width: 70,
                      height: 70,
                      borderRadius: 35,
                      borderWidth: 2.5,
                      borderColor: '#06a88e',
                      marginTop: 10,
                      marginLeft: 20,
                    }}
                    source={{uri: item.uri}}
                  />

                  <Text
                    style={{
                      marginTop: 10,
                      marginLeft: 20,
                      marginBottom: 60,
                      fontSize: 14,
                      color: '#000',
                      textAlign: 'center',
                    }}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          }}
        />
        <FlatList
          style={{
            backgroundColor: 'white',
          }}
          data={chatHistory}
          ItemSeparatorComponent={renderSeparator}
          renderItem={renderItem}></FlatList>
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  menuCommon_img: {
    width: 70,
    height: 70,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuCommon_text: {
    marginBottom: 60,
    fontSize: 14,
    color: '#000',
  },
  menuCommon: {
    flexDirection: 'column',
    alignItems: 'center',
    // backgroundColor: 'red',
    marginBottom: 20,
    marginRight: 40,
    height: 150,
  },
});
