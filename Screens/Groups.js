import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const itemWidth = windowWidth - 60;
export default function Groups({navigation}) {
  const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#CED0CE',
        }}
      />
    );
  };
  function ChatItem({dataItem}) {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
          marginTop: 10,
        }}>
        <View style={{flexDirection: 'row'}}>
       <TouchableOpacity  onPress={() => navigation.navigate('Message',{dataItem})}>
       <Image
            style={{
              width: 60,
              height: 60,
              resizeMode: 'cover',
              borderRadius: 30,
            }}
            source={{
              uri: dataItem.url,
            }}></Image>
       </TouchableOpacity  >
         
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity onPress={() => navigation.navigate('Message',{dataItem})}>
            <Text
              style={{
                marginLeft: 20,
                fontWeight: 'bold',
                fontSize: 18,
                alignSelf: 'center',
              }}>
              {dataItem.name}
            </Text>
            <Text
              style={{
                marginLeft: 20,

                fontSize: 10,
              }}>
              {dataItem.firstmessage}
            </Text>
            </TouchableOpacity>
         
          </View>
        </View>
        <View style={{flexDirection: 'row', marginRight:10}}>
          <TouchableOpacity onPress={() => navigation.navigate('CallWait',{dataItem}) }>
            <Ionicons
              style={{marginRight: 20}}
              name="ios-call"
              size={30}
              color={'#06a88e'}></Ionicons>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate('CallWait',{dataItem}) }>
            <Ionicons
              style={{}}
              name="videocam"
              size={30}
              color={'#06a88e'}></Ionicons>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  const renderItem = ({item, index}) => {
    return <ChatItem dataItem={item} />;
  };
  const [chatHistory, setChatHistory] = useState([
    {
      url: 'https://randomuser.me/api/portraits/women/30.jpg',
      name: 'Nitu Desia',
      firstmessage: 'Hello,Wich',
      numberOfUnreadMessages: 12,
      timepass: '03:00 PM',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/20.jpg',
      name: 'Rohit',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 33,
      timepass: '01:45 PM',
      isRead: false,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/22.jpg',
      name: 'Marcello Di Giovanni',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 14,
      timepass: '01:52 PM',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/24.jpg',
      name: 'Amanda4',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 0,
      timepass: 'Yesterday',
      isRead: false,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/26.jpg',
      name: 'Amanda5',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: 'Yesterday',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/26.jpg',
      name: 'Amanda6',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '2 minute ago',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/31.jpg',
      name: 'Amanda7',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '4 minute ago',
      isRead: false,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/30.jpg',
      name: 'Amanda',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '5 minute ago',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/29.jpg',
      name: 'Amanda',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '4 minute ago',
      isRead: false,
    },
  ]);

  return (
    <View>
      <View style={{height: 100, marginLeft: 10}}>
        <TouchableOpacity>
          <Text style={{color: '#06a88e', fontSize: 16, marginBottom: 10}}>
            Create New Group Call
          </Text>
        </TouchableOpacity>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Ionicons
              name="people"
              style={{
                width: 60,
                height: 60,
                backgroundColor: '#06a88e',
                textAlign: 'center',
                padding:10,
                borderRadius: 30,
              }}
              size={30}
              color={'white'}></Ionicons>
            <Text
              style={{
                marginLeft: 20,
                fontWeight: 'bold',
                fontSize: 18,
                alignSelf: 'center',
              }}>
              New Group
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginRight:10}}>
            <TouchableOpacity>
            <Ionicons
              style={{marginRight: 20}}
              name="ios-call"
              size={30}
              color={'#06a88e'}></Ionicons>
            </TouchableOpacity>
            
<TouchableOpacity>
<Ionicons
             
              name="videocam"
              size={30}
              color={'#06a88e'}></Ionicons>
</TouchableOpacity>
            
          </View>
        </View>
      </View>
      <View style={{ marginLeft: 10}}>
        <TouchableOpacity>
          <Text style={{color: '#06a88e', fontSize: 16, marginBottom: 10}}>
            Create New Call
          </Text>
        </TouchableOpacity>
        <FlatList
          style={{
            backgroundColor: 'white',
          height:windowHeight,
            
          }}
          data={chatHistory}
          ItemSeparatorComponent={renderSeparator}
          renderItem={renderItem}></FlatList>
      </View>
    </View>
  );
}
