import {
  View,
  Text,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  SafeAreaView,
  LogBox,
} from 'react-native';

import React, {useState, useEffect, useRef} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  firebaseDatabase,
  firebaseDatabaseRef,
  firebaseSet,
  onValue,
  push,
} from '../firebase/firebase';
import {child, get, off} from 'firebase/database';
import {firebase} from '@react-native-firebase/database';
import database from '@react-native-firebase/database';
import VirtualizedView from '../Components/VirtualizedView';
const slideWidth = Dimensions.get('window').width;
const slideHeight = Dimensions.get('window').height;
const reference = firebase
  .app()
  .database('https://chatapprn-a15a9-default-rtdb.firebaseio.com/');
export default function Message({navigation: {goBack}, route}) {
  const [typedText, setTypedText] = useState('');
  const [checkSender, setCheckSender] = useState(true);
  const [chatHistory, setChatHistory] = useState([]);
  const yourRef = useRef(null);
  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);
  useEffect(() => {
    database()
      .ref('chats')
      .on('value', async snapshot => {
        if (snapshot.exists()) {
          let snapshotObject = snapshot.val();
          let userLoggedId = await AsyncStorage.getItem('loggedUser');
          console.log(userLoggedId);
          const keyCurrentChat = Object.keys(snapshotObject).filter(item => {
            return (
              (item.split('-')[0] === JSON.parse(userLoggedId) &&
                item.split('-')[1] === route.params.dataItem.uid) ||
              (item.split('-')[1] === JSON.parse(userLoggedId) &&
                item.split('-')[0] === route.params.dataItem.uid)
            );
          });

          let updatedChatHistory = Object.entries(
            snapshotObject[keyCurrentChat[0]],
          )
            .map(item => {
              return item[1];
            })
            .sort((a, b) => a.timestamp - b.timestamp);
          if (
            keyCurrentChat &&
            keyCurrentChat[0].split('-')[1] === JSON.parse(userLoggedId) &&
            keyCurrentChat &&
            keyCurrentChat[0].split('-')[0] === route.params.dataItem.uid
          ) {
            updatedChatHistory = updatedChatHistory.map(item => {
              setCheckSender(false);
              return {...item, isSender: !item.isSender};
            });
          }
          for (let i = 0; i < updatedChatHistory.length; i++) {
            let item = updatedChatHistory[i];
            if (i == 0) {
              item.showUrl = true;
            } else {
              if (item.isSender === updatedChatHistory[i - 1].isSender) {
                item.showUrl = false;
              }
            }
          }
          setChatHistory(updatedChatHistory);
        } else {
          console.log('No Data available ');
        }
      });
  }, []);

  const renderItem = ({item, index}) => {
    return <ChatItem dataItem={item} key={`${item.timestamp}`} />;
  };

  const sendMessage = async () => {
    try {
      if (typedText.trim().length === 0) {
        return;
      }
      let senderId = JSON.parse(await AsyncStorage.getItem('loggedUser'));
      let receiverId = route.params.dataItem.uid;
      const messageRef = firebaseDatabaseRef(
        firebaseDatabase,
        `chats/${checkSender ? senderId : receiverId}-${
          checkSender ? receiverId : senderId
        }`,
      );
      const newMessageRef = push(messageRef);
      firebaseSet(newMessageRef, {
        url: 'https://randomuser.me/api/portraits/men/50.jpg',
        showUrl: true,
        isSender: checkSender,
        messenger: typedText,
        timestamp: new Date().getTime(),
      }).then(() => {
        setTypedText('');
      });
    } catch (err) {
      console.log(err);
    }
  };

  function ChatItem({dataItem}) {
    return dataItem.isSender == false ? (
      <TouchableOpacity
        style={{
          marginTop: 5,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        {dataItem.showUrl == true ? (
          <Image
            style={{
              width: 40,
              height: 40,
              resizeMode: 'cover',
              borderRadius: 20,
              marginRight: 15,
              marginStart: 10,
            }}
            source={{
              uri: dataItem.url,
            }}
          />
        ) : (
          <View
            style={{
              width: 40,
              height: 40,
              marginRight: 15,
              marginStart: 10,
            }}
          />
        )}
        <View
          style={{
            width: slideWidth * 0.7,
            flexDirection: 'row',
          }}>
          <View>
            <Text
              style={{
                color: 'black',
                fontSize: 16,
                paddingVertical: 5,
                paddingHorizontal: 7,
                backgroundColor: '#f2f2f2',
                borderRadius: 10,
              }}>
              {dataItem.messenger}
            </Text>
          </View>
          <View style={{width: 20}}></View>
        </View>
        {/* isSender = true */}
      </TouchableOpacity>
    ) : (
      <TouchableOpacity
        style={{
          marginTop: 5,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: slideWidth * 0.7,
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          <View style={{width: 40}}></View>
          <View>
            <Text
              style={{
                color: 'black',
                fontSize: 16,
                paddingVertical: 5,
                paddingHorizontal: 7,
                backgroundColor: '#00c0a5',
                borderRadius: 10,
              }}>
              {dataItem.messenger}
            </Text>
          </View>
        </View>
        {dataItem.showUrl == true ? (
          <Image
            style={{
              width: 40,
              height: 40,
              resizeMode: 'cover',
              borderRadius: 20,
              marginRight: 15,
              marginStart: 10,
            }}
            source={{
              uri: dataItem.url,
            }}
          />
        ) : (
          <View
            style={{
              width: 40,
              height: 40,
              marginRight: 15,
              marginStart: 10,
            }}
          />
        )}
      </TouchableOpacity>
    );
  }
  const scrollViewRef = useRef();
  return (
    <SafeAreaView style={{height: slideHeight, flex: 1}}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: 'white',
          }}>
          <View style={{flexDirection: 'row', padding: 5}}>
            <TouchableOpacity onPress={() => goBack()}>
              <Ionicons
                style={{padding: 5}}
                name="ios-arrow-back-sharp"
                size={25}
                color={'#06a88e'}></Ionicons>
            </TouchableOpacity>
            <Image
              style={{
                width: 50,
                height: 50,
                resizeMode: 'cover',
                borderRadius: 30,
                marginRight: 15,
                marginStart: 10,
              }}
              source={{
                uri: route?.params.dataItem.url,
              }}></Image>
            <Text style={{fontSize: 16, fontWeight: '500', lineHeight: 40}}>
              {route?.params.dataItem.name}
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
              <Ionicons
                style={{padding: 10}}
                name="videocam"
                size={25}
                color={'#06a88e'}></Ionicons>
            </TouchableOpacity>

            <TouchableOpacity>
              <Ionicons
                style={{padding: 10}}
                name="ios-call"
                size={25}
                color={'#06a88e'}></Ionicons>
            </TouchableOpacity>
            <TouchableOpacity>
              <Ionicons
                style={{padding: 10}}
                name="ellipsis-horizontal-circle-sharp"
                size={25}
                color={'#06a88e'}></Ionicons>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{height: slideHeight - 150}}>
          <FlatList
            ref={yourRef}
            nestedScrollEnabled
            data={chatHistory}
            renderItem={renderItem}
            onContentSizeChange={() => yourRef.current.scrollToEnd()}
            onLayout={() => yourRef.current.scrollToEnd()}
          />
        </View>
        <View
          style={{
            width: slideWidth,
            height: 70,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={{marginLeft: 10}}>
              <Ionicons
                name="md-happy-outline"
                size={25}
                style={{lineHeight: 50}}></Ionicons>
            </TouchableOpacity>

            <TextInput
              style={{
                color: 'black',
                paddingStart: 10,
              }}
              value={typedText}
              placeholder="Type your message"
              placeholderTextColor={'grey'}
              onChangeText={text => setTypedText(text)}
            />
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
              <Ionicons
                style={{padding: 10}}
                name="ios-document-attach-outline"
                size={25}></Ionicons>
            </TouchableOpacity>
            <TouchableOpacity>
              <Ionicons style={{padding: 10}} name="camera" size={25} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                sendMessage();
              }}>
              <Ionicons
                style={{padding: 10}}
                name="send"
                size={25}
                color={'#06a88e'}
              />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
