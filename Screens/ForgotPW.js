import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import InputCommon from '../Components/InputCommon';
import {DIMENSIONS} from '../Styles/Dimensions';
import ButtonCommon from '../Components/ButtonCommon';
import BackButton from '../Components/BackButton';
import {auth, sendPasswordResetEmail} from '../firebase/firebase';
import Loading from './Loading';

export default function ForgotPW({navigation}) {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const handleResetPassword = email => {
    setLoading(true);
    if (email !== '') {
      sendPasswordResetEmail(auth, email)
        .then(() => {
          Alert.alert('Alert', 'Send password reset successfully!');
        })
        .catch(err => {
          console.log(err);
          Alert.alert('Warning', 'Error to send password reset email');
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      Alert.alert('Warning', 'You need to enter your email!');
    }
  };

  return loading ? (
    <Loading />
  ) : (
    <ScrollView style={styles.container01}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <BackButton />
      </TouchableOpacity>
      <View style={styles.container02}>
        <View style={styles.subContainer}>
          <Text style={styles.label}>Enter your email</Text>
          <InputCommon
            iconName={'person'}
            iconSize={20}
            iconColor={'black'}
            placeholder={'Email...'}
            value={email}
            onChangeText={txt => setEmail(txt)}
          />
          <TouchableOpacity onPress={() => handleResetPassword(email)}>
            <ButtonCommon btnName={'Send reset password'} />
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container01: {
    flex: 1,
    backgroundColor: 'white',
  },
  container02: {
    alignItems: 'center',
  },
  subContainer: {
    width: DIMENSIONS.screenWidth / 1.2,
    alignItems: 'center',
    marginTop: DIMENSIONS.screenWidth / 15,
  },
  label: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
    marginLeft: DIMENSIONS.screenWidth / 20,
  },
});
