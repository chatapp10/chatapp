import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import InputCommon from '../Components/InputCommon';
import ButtonCommon from '../Components/ButtonCommon';
import LogoCommon from '../Components/LogoCommon';
import {DIMENSIONS} from '../Styles/Dimensions';
import {
  signInWithEmailAndPassword,
  auth,
  onAuthStateChanged,
  firebaseSet,
  firebaseDatabaseRef,
  firebaseDatabase,
  signInWithCredential,
  FacebookAuthProvider,
  get,
  child,
  GoogleAuthProvider
} from '../firebase/firebase';
import Loading from './Loading';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {LoginManager, AccessToken} from 'react-native-fbsdk-next';

export default function SignIn({navigation}) {
  const [email, setEmail] = useState('minhluu15012000@gmail.com');
  const [userGoogle, setUserGoogle] = useState({});
  const [password, setPassword] = useState('123456');
  const [loading, setLoading] = useState(false);

  const handleExistUser = async () => {
    const user = await AsyncStorage.getItem('loggedUser');
    if (user !== null) {
      navigation.navigate('Favorite');
    }
  };
  useEffect(() => {
    handleExistUser();
  }, []);

  useEffect(() => {
    onAuthStateChanged(auth, user => {
      console.log('ok');
      if (user) {
        const userId = user.uid;
        get(
          child(firebaseDatabaseRef(firebaseDatabase), `users/${userId}`),
        ).then(snapshot => {
          if (snapshot.exists()) {
            console.log("ok2")
            firebaseSet(
              firebaseDatabaseRef(firebaseDatabase, `users/${userId}`),
              snapshot.val(),
            );
          }
        });
      }
    });
  });

  useEffect(() => {
    GoogleSignin.configure({
      webClientId:
        '939340392096-81abmpje6bnilf5bv7gkp4jemorr2ks4.apps.googleusercontent.com',
    });
  }, []);

  const handleSignIn = async () => {
    if (email !== '' && password !== '') {
      setLoading(true);
      signInWithEmailAndPassword(auth, email, password)
        .then(async userCredential => {
          const user = userCredential.user;
          console.log(user);
          if (user.emailVerified) {
            setLoading(false);
            await AsyncStorage.setItem('loggedUser', JSON.stringify(user.uid));
            navigation.navigate('Home');
          } else {
            setLoading(false);
            Alert.alert('Warning', 'Your email is not verified!');
          }
        })
        .catch(() => {
          setLoading(false);
          Alert.alert('Warning', 'Invalid email or password');
        });
    } else {
      Alert.alert('Warning', 'Please fill out the fields completely!');
    }
  };

  const googleSignIn = async () => {
    setLoading(true);

    await GoogleSignin.signIn()
      .then(async infor => {
        console.log('infor', infor);

        await AsyncStorage.setItem('loggedUser', JSON.stringify(infor.user.id));
        firebaseSet(
          firebaseDatabaseRef(firebaseDatabase, `users/${infor.user.id}`),
          {
            email: infor?.user.email,
            emailVerified: true,
            accessToken: infor?.idToken,
            friends: 0,
            userName: infor?.user?.name,
          },
        );
        return infor;
      })
      .then(infor => {
        const googleCredential = GoogleAuthProvider.credential(infor.idToken);
        return googleCredential;
      })
      .then(infor => {
        signInWithCredential(auth, infor)
          .then(() => {
            navigation.navigate('Home');
          })
          .catch(error => {
            console.log(error);
          })
          .finally(() => {
            setLoading(false);
          });
      })
      .catch(e => {
        console.log(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const onFacebookButtonPress = async () => {
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);
    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }
    const data = await AccessToken.getCurrentAccessToken();
    if (!data) {
      throw 'Something went wrong obtaining access token';
    }
    const facebookCredential = FacebookAuthProvider.credential(
      data.accessToken,
    );
    await signInWithCredential(auth, facebookCredential)
      .then(async infor => {
        console.log(infor);
        await AsyncStorage.setItem('loggedUser', JSON.stringify(user.uid));
        navigation.navigate('Home');
      })
      .catch(error => console.log(error));
  };

  const signUp = () => {
    navigation.navigate('SignUp');
  };

  return (
    <ScrollView style={[styles.containerSI, {opacity: loading ? 0.5 : 1}]}>
      <LogoCommon />
      <View style={styles.inputContainerSI}>
        <InputCommon
          iconName={'person'}
          iconSize={20}
          iconColor={'black'}
          placeholder={'Email...'}
          value={email}
          onChangeText={txt => setEmail(txt)}
        />
        <InputCommon
          iconName={'lock-closed'}
          iconSize={20}
          iconColor={'black'}
          placeholder={'Password...'}
          value={password}
          onChangeText={txt => setPassword(txt)}
          secureTextEntry={true}
        />
        <TouchableOpacity onPress={() => handleSignIn()}>
          <ButtonCommon btnName={'Sign In'} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('ForgotPW')}>
          <Text style={styles.ForgotPW}>Forgot Password?</Text>
        </TouchableOpacity>
        <Text style={styles.ForgotPW}>Sign in with</Text>
        <View style={styles.otherLoginSI}>
          <TouchableOpacity onPress={() => onFacebookButtonPress()}>
            <Ionicons
              style={styles.otherLoginLogoSI}
              size={20}
              name={'logo-facebook'}
              color={'#4267B2'}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => googleSignIn()}>
            <Ionicons
              style={styles.otherLoginLogoSI}
              size={20}
              name={'logo-google'}
              color={'#DB4437'}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => signUp()}>
          <Text style={styles.ForgotPW}>
            Don't have an account? Create here
          </Text>
        </TouchableOpacity>
      </View>
      {loading && <Loading />}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  // SignInScreen
  containerSI: {
    backgroundColor: 'white',
    flex: 1,
  },
  inputContainerSI: {
    alignItems: 'center',
  },
  ForgotPW: {
    marginTop: DIMENSIONS.screenWidth / 22,
    fontSize: 15,
    fontWeight: 'bold',
    color: 'black',
  },
  otherLoginSI: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: DIMENSIONS.screenWidth / 4,
    marginTop: DIMENSIONS.screenWidth / 30,
  },
  otherLoginLogoSI: {
    borderWidth: 1,
    borderColor: 'black',
    padding: DIMENSIONS.screenWidth / 40,
    borderRadius: 20,
  },
});
