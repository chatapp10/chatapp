import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import React, {useState, useEffect} from 'react'

const windowWidth = Dimensions.get('window').width
const itemWidth = windowWidth - 60

export default function ProfileAccount ({navigation, route}) {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          height: 60,
        }}
      >
        <View style={{flexDirection: 'row', padding: 5}}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons
              style={{padding: 5}}
              name='ios-arrow-back-sharp'
              size={25}
              color={'#06a88e'}
            ></Ionicons>
          </TouchableOpacity>

          <Text style={{fontSize: 20, fontWeight: '500', lineHeight: 35}}>
            My Profile
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <Ionicons
              style={{padding: 10}}
              name='ellipsis-horizontal-circle-sharp'
              size={25}
              color={'#06a88e'}
            ></Ionicons>
          </TouchableOpacity>
        </View>
      </View>
      <View>
        <Image
          style={{
            width: windowWidth,
            height: 350,
            resizeMode: 'cover',
            position: 'relative',
          }}
          source={{
            uri: route?.params.ProfileAccount.url,
          }}
        ></Image>

        <Text
          style={{
            position: 'absolute',
            color: 'white',
            bottom: 0,
            marginBottom: 30,
            marginLeft: 10,
            fontSize: 20,
            fontWeight: '700',
          }}
        >
          {route?.params.ProfileAccount.name}  {route?.params.ProfileAccount.firstName}
        </Text>
        <TouchableOpacity>
        <View style={{width:50,height:50,backgroundColor:'#06a88e', borderColor:'white',borderWidth:2,borderRadius:25,position:'absolute',bottom:0,right:0,marginBottom:20,alignItems:'center'}}>
        <Ionicons style={{lineHeight:50}} name='camera' size={30}  color={'white'}></Ionicons>
      </View>
        </TouchableOpacity>
   
      </View>
      <View style={{flexDirection: 'column',marginTop:30}}>
            <TouchableOpacity >
            <Text
              style={{
                marginLeft: 20,
                fontWeight: 'bold',
                fontSize: 18,
               color:'black'
              }}>
             About and phone number
            </Text>
            <Text
              style={{
                marginLeft: 20,

                fontSize: 15,
              }}>
             At the work
            </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop:30}} >
            <Text
              style={{
                marginLeft: 20,
                fontWeight: 'bold',
                fontSize: 18,
               color:'black'
              }}>
             Change Number
            </Text>
            <Text
              style={{
                marginLeft: 20,

                fontSize: 15,
              }}>
             +84 0362306429
            </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop:30}}>
            <Text
              style={{
                marginLeft: 20,
                fontWeight: 'bold',
                fontSize: 18,
               color:'black'
              }}>
           My last seen
            </Text>
            <Text
              style={{
                marginLeft: 20,

                fontSize: 15,
              }}>
            Nobody
            </Text>
            </TouchableOpacity>
         
          </View>
    </View>
  )
}
