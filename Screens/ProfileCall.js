import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import React, {useState, useEffect} from 'react'

const windowWidth = Dimensions.get('window').width
const itemWidth = windowWidth - 60

export default function ProfileCall ({navigation: {goBack}, route}) {
  return (
    <View style={{flexDirection: 'column', alignItems: 'center'}}>
      <Image
        style={{
          width: windowWidth,

          resizeMode: 'cover',
          height: 500,
        }}
        source={{
          uri: route?.params.dataItem.url,
        }}
      />

      <Text style={{fontSize: 25, fontWeight: '700'}}>
        {route?.params.dataItem.name}
      </Text>
      <View style={{flexDirection: 'row'}}>
        <Ionicons></Ionicons>
        <Text style={{fontSize: 12}}>Whatsapp Calling</Text>
      </View>
      <View style={{flexDirection: 'row', justifyContent:'space-around',width:windowWidth-60, marginTop:20,marginBottom:20}}>
        <TouchableOpacity
          style={{
            backgroundColor: 'white',
            width: 60,
            height: 60,
            alignItems: 'center',
            borderWidth:2,
            borderColor:'#06a88e',
            borderRadius: 30,
          }}
         
        >
         
          <Ionicons
            style={{lineHeight: 60}}
            name='ios-mic-off-sharp'
            size={25}
            color={'#06a88e'}
          ></Ionicons>
        </TouchableOpacity>
        <TouchableOpacity
         style={{
            backgroundColor: 'white',
            width: 60,
            height: 60,
            alignItems: 'center',
            borderWidth:2,
            borderColor:'#06a88e',
            borderRadius: 30,
          }}
        >
         
          <Ionicons
            style={{lineHeight: 50}}
            name='ios-volume-medium-sharp'
            size={25}
            color={'#06a88e'}
          ></Ionicons>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: 'white',
            width: 60,
            height: 60,
            alignItems: 'center',
            borderWidth:2,
            borderColor:'#06a88e',
            borderRadius: 30,
          }}
        >
         
          <Ionicons
            style={{lineHeight: 60}}
            name='videocam'
            size={25}
            color={'#06a88e'}
          ></Ionicons>
        </TouchableOpacity>
        <TouchableOpacity
        style={{
            backgroundColor: 'white',
            width: 60,
            height: 60,
            alignItems: 'center',
            borderWidth:2,
            borderColor:'#06a88e',
            borderRadius: 30,
          }}
        >
         
          <Ionicons
            style={{lineHeight: 60}}
            name='call-sharp'
            size={20}
            color={'#06a88e'}
          ></Ionicons>
        </TouchableOpacity>
      </View>
      <TouchableOpacity  onPress={() => goBack()}>
        <View
          style={{
            backgroundColor: 'red',
            width: 60,
            height: 60,
            alignItems: 'center',
            borderRadius: 30,
          }}
        >
          <Ionicons
            style={{lineHeight: 60}}
            name='call-sharp'
            size={20}
            color={'white'}
          ></Ionicons>
        </View>
      </TouchableOpacity>
    </View>
  )
}
