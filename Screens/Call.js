import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const itemWidth = windowWidth - 60;

export default function Call({navigation}) {
  const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#CED0CE',
        }}
      />
    );
  };
  function ChatItem({dataItem}) {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
          marginTop: 10,
        }}>
        <View style={{flexDirection: 'row'}}>
       <TouchableOpacity>
       <Image
            style={{
              width: 60,
              height: 60,
              resizeMode: 'cover',
              borderRadius: 30,
            }}
            source={{
              uri: dataItem.url,
            }}></Image>
       </TouchableOpacity>
         
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity>
            <Text
              style={{
                marginLeft: 20,
                fontWeight: 'bold',
                fontSize: 18,
               
              }}>
              {dataItem.name}
            </Text>
            <View style={{flexDirection:'row'}}>
              <Ionicons style={{marginLeft:20}} name='trending-up-sharp' size={20} color={'#06a88e'}></Ionicons>
            <Text
              style={{
              marginLeft:10,

                fontSize: 15,
              }}>
            {dataItem.firstmessage}
            </Text>
            </View>
            </TouchableOpacity>
         
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('CallWait',{dataItem}) }>
          {dataItem.isCall==true ? (
            <Ionicons
              style={{marginRight: 20}}
              name="ios-call"
              size={30}
              color={'#06a88e'}></Ionicons>) : (<Ionicons
                style={{marginRight: 20}}
                name="ios-videocam"
                size={30}
                color={'#06a88e'}></Ionicons>) }
          </TouchableOpacity>

       
        </View>
      </View>
    );
  }
  const renderItem = ({item, index}) => {
    return <ChatItem dataItem={item} />;
  };
  const [chatHistory, setChatHistory] = useState([
    {
      url: 'https://randomuser.me/api/portraits/women/30.jpg',
      name: 'Nitu Desia',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 12,
      timepass: '03:00 PM',
      isCall: true,
     
    },
    {
      url: 'https://randomuser.me/api/portraits/women/20.jpg',
      name: 'Rohit',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 33,
      timepass: '01:45 PM',
      isCall: false,
    
    },
    {
      url: 'https://randomuser.me/api/portraits/women/22.jpg',
      name: 'Marcello Di Giovanni',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 14,
      timepass: '01:52 PM',
      isCall: true,
     
    },
    {
      url: 'https://randomuser.me/api/portraits/women/24.jpg',
      name: 'Amanda4',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 0,
      timepass: 'Yesterday',
      isCall: false,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/26.jpg',
      name: 'Amanda5',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: 'Yesterday',
      isCall: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/26.jpg',
      name: 'Amanda6',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: '2 minute ago',
      isCall: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/31.jpg',
      name: 'Amanda7',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: '4 minute ago',
      isCall: false,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/30.jpg',
      name: 'Amanda',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: '5 minute ago',
      isCall: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/29.jpg',
      name: 'Amanda',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '4 minute ago',
      isCall: false,
    },
  ]);

  return (
    <View>
      <View style={{height: 100, marginLeft: 10}}>
        <TouchableOpacity>
          <Text style={{color: 'red', fontSize: 16, marginBottom: 10}}>
          Miss Call
          </Text>
        </TouchableOpacity>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
          <Image
            style={{
              width: 60,
              height: 60,
              resizeMode: 'cover',
              borderRadius: 30,
            }}
            source={{
              uri: 'https://randomuser.me/api/portraits/women/29.jpg',
            }}></Image>
       </TouchableOpacity>
         
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity>
            <Text
              style={{
                marginLeft: 20,
                fontWeight: 'bold',
                fontSize: 18,
                
              }}>
            Amanda
            </Text>
            <View style={{flexDirection:'row'}}>
              <Ionicons style={{marginLeft:20}} name='trending-down-sharp' size={20} color={'red'}></Ionicons>
            <Text
              style={{
              marginLeft:10,

                fontSize: 15,
              }}>
             what is it
            </Text>
            </View>
           
            </TouchableOpacity>
         
          </View>
          </View>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
            <Ionicons
              style={{marginRight: 20}}
              name="ios-call"
              size={30}
              color={'#06a88e'}></Ionicons>
            </TouchableOpacity>
            

            
          </View>
        </View>
      </View>
      <View style={{height: windowHeight - 100, marginLeft: 10}}>
        <TouchableOpacity>
          <Text style={{color: '#06a88e', fontSize: 16, marginBottom: 10}}>
           Orthers Call
          </Text>
        </TouchableOpacity>
        <FlatList
          style={{
            backgroundColor: 'white',
          }}
          data={chatHistory}
          ItemSeparatorComponent={renderSeparator}
          renderItem={renderItem}></FlatList>
      </View>
    </View>
  );
}