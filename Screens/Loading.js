import {View, StyleSheet, ActivityIndicator} from 'react-native';
import React from 'react';
import {DIMENSIONS} from '../Styles/Dimensions';

export default function Loading() {
  return (
    <View style={styles.loadingContainer}>
      <ActivityIndicator size={40} color={'black'} />
    </View>
  );
}

const styles = StyleSheet.create({
  loadingContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    width: DIMENSIONS.screenWidth,
    height: DIMENSIONS.screenHeight,
  }
});
