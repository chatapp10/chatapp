import {
    View,
    Text,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
  } from 'react-native';
  import React, {useState, useEffect} from 'react';
  import Ionicons from 'react-native-vector-icons/Ionicons';
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const itemWidth = windowWidth - 60;
export default function ProfileFriend({navigation,route}) {
    
  
    const renderItemimg = ({item, index}) => {
        return <Imgstatus dataItem={item} />;
      };
      function Imgstatus({dataItem}) {
        return (
          <View style={{flexDirection:'row'}}>
            <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg1,
                  }}></Image>
                   <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg2,
                  }}></Image>
                   <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg3,
                  }}></Image>
                   <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg4,
                  }}></Image>
                   <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg5,
                  }}></Image>
          </View>
        
        );
      }
      const renderItemimg1 = ({item, index}) => {
        return <Imgstatus1 dataItem={item} />;
      };
      function Imgstatus1({dataItem}) {
        return (
          <View style={{flexDirection:'row'}}>
            <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg1,
                  }}></Image>
                   <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg2,
                  }}></Image>
                   <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg3,
                  }}></Image>
                   <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg4,
                  }}></Image>
                   <Image  style={{width:100,height:100,marginRight:10}} source={{
                    uri: dataItem.urlimg5,
                  }}></Image>
          </View>
        
        );
      }
  return (
    <View>
       <View>
        <Image
          style={{
            width: windowWidth,
            height: 450,
            resizeMode: 'cover',
            position: 'relative',
          }}
          source={{
            uri: route?.params.dataItem.urlavar,
          }}
        ></Image>

        <Text
          style={{
            position: 'absolute',
            color: 'white',
            bottom: 40,
            marginBottom: 30,
            marginLeft: 10,
            fontSize: 20,
            fontWeight: '700',
          }}
        >
          {route?.params.dataItem.name} 
        </Text>
        <Text
          style={{
            position: 'absolute',
            color: 'white',
            bottom: 40,
            
            marginLeft: 10,
            fontSize: 12,
            fontWeight: '700',
          }}
        >
         Last seen {route?.params.dataItem.firstmessage} 
        </Text>
        
        <TouchableOpacity>
        <View style={{width:30,height:30,backgroundColor:'#06a88e', borderColor:'white',borderWidth:2,borderRadius:25,position:'absolute',bottom:0,right:0,marginBottom:20,alignItems:'center',}}>
        <Ionicons style={{lineHeight:30}} name='camera' size={15}  color={'white'}></Ionicons>
      </View>
        </TouchableOpacity>
        <TouchableOpacity>
        <View style={{width:30,height:30,backgroundColor:'#06a88e', borderColor:'white',borderWidth:2,borderRadius:25,position:'absolute',bottom:0,right:30,marginBottom:20,alignItems:'center',marginRight:10}}>
        <Ionicons style={{lineHeight:30}} name='videocam' size={15}  color={'white'}></Ionicons>
      </View>
        </TouchableOpacity>
        <TouchableOpacity>
        <View style={{width:30,height:30,backgroundColor:'#06a88e', borderColor:'white',borderWidth:2,borderRadius:25,position:'absolute',bottom:0,right:60,marginBottom:20,alignItems:'center',marginRight:20}}>
        <Ionicons style={{lineHeight:30}} name='ios-call' size={15}  color={'white'}></Ionicons>
      </View>
        </TouchableOpacity>
   
      </View>
      <View style={{flexDirection:'row',justifyContent:'space-between',windowWidth:windowWidth,marginTop:20}}>
        <Text style={{fontWeight:'700'}}> Media</Text>
        <Text style={{fontWeight:'700',marginRight:20}}>78</Text>
      </View>
      <FlatList
        style={{flexDirection:'row',width:windowWidth,height:100,marginTop:30}}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
        data={route?.params.dataItem.img1}
       
        renderItem={renderItemimg}></FlatList>
        <FlatList
        style={{flexDirection:'row',width:windowWidth,height:100,marginTop:20}}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
        data={route?.params.dataItem.img1}
       
        renderItem={renderItemimg}></FlatList>
        
    </View>
  )
}