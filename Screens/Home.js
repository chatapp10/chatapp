import {View, Text, TouchableOpacity} from 'react-native';
import React, { useEffect } from 'react';
import {auth, signOut} from '../firebase/firebase';

export default function Home({navigation}) {
  useEffect(() => {
    console.log(auth.currentUser);
  }, [])

  const handleSignOut = () => {
    signOut(auth).then(() => {
      navigation.navigate('SignIn');
    }).catch((error) => {
      console.log(error.message);
    });
  }
  return (
    <TouchableOpacity onPress={() => handleSignOut()}>
      <View style={{backgroundColor: 'blue', alignItems: 'center', padding: 30}}>
        <Text style={{color: 'white', fontSize: 30}}>Sign out</Text>
      </View>
    </TouchableOpacity>
  );
}
