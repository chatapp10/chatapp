import {
  View,
  TextInput,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import React, {useState, useEffect} from 'react'

const windowWidth = Dimensions.get('window').width
const itemWidth = windowWidth - 60

export default function Search ({navigation}) {
  const [filteredDataSource, setFilteredDataSource] = useState([])
  const [masterDataSource, setMasterDataSource] = useState([])
  const [search, setSearch] = useState('')
  useEffect(() => {
    setFilteredDataSource(chatHistory)
    setMasterDataSource(chatHistory)
  }, [])
  const searchFilterFunction = text => {
   
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource and update FilteredDataSource
      const newData = masterDataSource.filter(function (item) {
        // Applying filter for the inserted text in search bar
        const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase()
        const textData = text.toUpperCase()
        return itemData.indexOf(textData) > -1
      })
      setFilteredDataSource(newData)
      setSearch(text)
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredDataSource(masterDataSource)
      // setall(masterDataSource);
      setSearch(text)
    }
  }
  const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#CED0CE',
        }}
      />
    )
  }
  function ChatItem ({dataItem}) {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
          marginTop: 10,
          marginLeft: 20,
        }}
      >
        <View style={{flexDirection: 'row', width: windowWidth}}>
          <TouchableOpacity>
            <Image
              style={{
                width: 60,
                height: 60,
                resizeMode: 'cover',
                borderRadius: 30,
              }}
              source={{
                uri: dataItem.url,
              }}
            ></Image>
          </TouchableOpacity>

          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity
              onPress={() => navigation.navigate('Message', {dataItem})}
            >
              <Text
                style={{
                  marginLeft: 20,
                  fontWeight: 'bold',
                  fontSize: 18,
                  alignSelf: 'center',
                }}
              >
                {dataItem.name}
              </Text>
              <Text
                style={{
                  marginLeft: 20,

                  fontSize: 10,
                }}
              >
                {dataItem.firstmessage}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
  const renderItem = ({item, index}) => {
    return <ChatItem dataItem={item} />
  }
  const [chatHistory, setChatHistory] = useState([
    {
      url: 'https://randomuser.me/api/portraits/women/30.jpg',
      name: 'Nitu Desia',
      firstmessage: 'Hello,Wich',
      numberOfUnreadMessages: 12,
      timepass: '03:00 PM',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/20.jpg',
      name: 'Rohit',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 33,
      timepass: '01:45 PM',
      isRead: false,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/22.jpg',
      name: 'Marcello Di Giovanni',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 14,
      timepass: '01:52 PM',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/24.jpg',
      name: 'Amanda4',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 0,
      timepass: 'Yesterday',
      isRead: false,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/26.jpg',
      name: 'Amanda5',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: 'Yesterday',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/26.jpg',
      name: 'Amanda6',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '2 minute ago',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/31.jpg',
      name: 'Amanda7',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '4 minute ago',
      isRead: false,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/30.jpg',
      name: 'Amanda',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '5 minute ago',
      isRead: true,
    },
    {
      url: 'https://randomuser.me/api/portraits/women/29.jpg',
      name: 'Amanda',
      firstmessage: 'Hello,It me',
      numberOfUnreadMessages: 22,
      timepass: '4 minute ago',
      isRead: false,
    },
  ])
  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons
            style={{padding: 15}}
            name='ios-arrow-back-sharp'
            size={25}
            color={'#06a88e'}
          ></Ionicons>
        </TouchableOpacity>
        <TextInput
           value={search}
           style={{width:windowWidth,lineHeight:40}}
           onChangeText={text => searchFilterFunction(text)}
           placeholderTextColor={'black'}
           placeholder="Typing Search..."
        >
         
        </TextInput>
      </View>
      <FlatList
        style={{
          backgroundColor: 'white',
        }}
        data={filteredDataSource}
        ItemSeparatorComponent={renderSeparator}
        renderItem={renderItem}
      ></FlatList>
    </View>
  )
}
