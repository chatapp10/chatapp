import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import React, {useState, useEffect} from 'react'

const windowWidth = Dimensions.get('window').width
const itemWidth = windowWidth - 60

export default function Setting ({navigation: {goBack}}) {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          height: 60,
        }}
      >
        <View style={{flexDirection: 'row', padding: 5}}>
          <TouchableOpacity onPress={() => goBack()}>
            <Ionicons
              style={{padding: 5}}
              name='ios-arrow-back-sharp'
              size={25}
              color={'#06a88e'}
            ></Ionicons>
          </TouchableOpacity>

          <Text style={{fontSize: 20, fontWeight: '500', lineHeight: 35}}>
            Setting
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <Ionicons
              style={{padding: 10}}
              name='ellipsis-horizontal-circle-sharp'
              size={25}
              color={'#06a88e'}
            ></Ionicons>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: 120,
          borderWidth: 0.5,
          borderColor: '#CED0CE',
        }}
      >
        <View style={{flexDirection: 'row', marginLeft: 10}}>
          <TouchableOpacity>
            <Image
              style={{
                width: 70,
                height: 70,
                resizeMode: 'cover',
                borderRadius: 35,
              }}
              source={{
                uri: 'https://randomuser.me/api/portraits/women/19.jpg',
              }}
            ></Image>
          </TouchableOpacity>

          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity>
              <Text
                style={{
                  marginLeft: 20,
                  fontWeight: 'bold',
                  fontSize: 18,
                }}
              >
                Raza Shakeeb
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    marginLeft: 20,
                    color: '#CED0CE',

                    fontSize: 15,
                  }}
                >
                  At work
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 5, padding: 20}}>
        <TouchableOpacity style={{flexDirection: 'row'}}>
          <Ionicons name='ios-build' color={'#06a88e'} size={30}></Ionicons>
          <Text style={{fontSize: 20, marginLeft: 20}}> Account</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 5, padding: 20}}>
        <TouchableOpacity style={{flexDirection: 'row'}}>
          <Ionicons
            name='chatbox-ellipses-sharp'
            color={'#06a88e'}
            size={30}
          ></Ionicons>
          <Text style={{fontSize: 20, marginLeft: 20}}> Chat</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 5, padding: 20}}>
      <TouchableOpacity style={{flexDirection: 'row'}}>
        <Ionicons
          name='notifications-sharp'
          color={'#06a88e'}
          size={30}
        ></Ionicons>
        <Text style={{fontSize: 20, marginLeft: 20}}> Notifications</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 5, padding: 20}}>
      <TouchableOpacity style={{flexDirection: 'row'}}>
        <Ionicons
          name='ios-reload-circle'
          color={'#06a88e'}
          size={30}
        ></Ionicons>
        <Text style={{fontSize: 20, marginLeft: 20}}> Data and storage usage</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 5, padding: 20}}>
      <TouchableOpacity style={{flexDirection: 'row'}}>
        <Ionicons name='person' color={'#06a88e'} size={30}></Ionicons>
        <Text style={{fontSize: 20, marginLeft: 20}}> Contacts</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 5, padding: 20}}>
        <TouchableOpacity style={{flexDirection: 'row'}}>
        <Ionicons name='card-sharp' color={'#06a88e'} size={30}></Ionicons>
        <Text style={{fontSize: 20, marginLeft: 20}}> Payment</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 5, padding: 20}}>
        <TouchableOpacity style={{flexDirection: 'row'}}>
        <Ionicons
          name='ios-people-sharp'
          color={'#06a88e'}
          size={30}
        ></Ionicons>
        <Text style={{fontSize: 20, marginLeft: 20}}> Invite Friend</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 5, padding: 20}}>
      <TouchableOpacity style={{flexDirection: 'row'}}>
        <Ionicons
          name='ios-help-outline'
          color={'#06a88e'}
          size={30}
        ></Ionicons>
        <Text style={{fontSize: 20, marginLeft: 20}}> About and Help</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
