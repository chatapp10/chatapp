import {
    View,
    Text,
    FlatList,
    Image,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight,
  } from 'react-native';
  import Ionicons from 'react-native-vector-icons/Ionicons';
  import React, {useState, useEffect} from 'react';
  const windowWidth = Dimensions.get('window').width;
export default function Contact({navigation}) {
    const renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: '100%',
              backgroundColor: '#CED0CE',
            }}
          />
        );
      };
      function ChatItem({dataItem}) {
        return (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 10,
              marginTop: 10,
              marginLeft:20
            }}>
            <View style={{flexDirection: 'row',width:windowWidth}}>
           <TouchableOpacity >
           <Image
                style={{
                  width: 60,
                  height: 60,
                  resizeMode: 'cover',
                  borderRadius: 30,
                }}
                source={{
                  uri: dataItem.url,
                }}></Image>
           </TouchableOpacity  >
             
              <View style={{flexDirection: 'column'}}>
                <TouchableOpacity    onPress={() => navigation.navigate('Message',{dataItem})}> 
                <Text
                  style={{
                    marginLeft: 20,
                    fontWeight: 'bold',
                    fontSize: 18,
                    alignSelf: 'center',
                  }}>
                  {dataItem.name}
                </Text>
                <Text
                  style={{
                    marginLeft: 20,
    
                    fontSize: 10,
                  }}>
                  {dataItem.firstmessage}
                </Text>
                </TouchableOpacity>
             
              </View>
            </View>
        
          </View>
        );
      }
      const renderItem = ({item, index}) => {
        return <ChatItem dataItem={item} />;
      };
    const [chatHistory, setChatHistory] = useState([
        {
          url: 'https://randomuser.me/api/portraits/women/30.jpg',
          name: 'Nitu Desia',
          firstmessage: 'Hello,Wich',
          numberOfUnreadMessages: 12,
          timepass: '03:00 PM',
          isRead: true,
        },
        {
          url: 'https://randomuser.me/api/portraits/women/20.jpg',
          name: 'Rohit',
          firstmessage: 'Hello,It me',
          numberOfUnreadMessages: 33,
          timepass: '01:45 PM',
          isRead: false,
        },
        {
          url: 'https://randomuser.me/api/portraits/women/22.jpg',
          name: 'Marcello Di Giovanni',
          firstmessage: 'Hello,It me',
          numberOfUnreadMessages: 14,
          timepass: '01:52 PM',
          isRead: true,
        },
        {
          url: 'https://randomuser.me/api/portraits/women/24.jpg',
          name: 'Amanda4',
          firstmessage: 'Hello,It me',
          numberOfUnreadMessages: 0,
          timepass: 'Yesterday',
          isRead: false,
        },
        {
          url: 'https://randomuser.me/api/portraits/women/26.jpg',
          name: 'Amanda5',
          firstmessage: 'Hello,It me',
          numberOfUnreadMessages: 22,
          timepass: 'Yesterday',
          isRead: true,
        },
        {
          url: 'https://randomuser.me/api/portraits/women/26.jpg',
          name: 'Amanda6',
          firstmessage: 'Hello,It me',
          numberOfUnreadMessages: 22,
          timepass: '2 minute ago',
          isRead: true,
        },
        {
          url: 'https://randomuser.me/api/portraits/women/31.jpg',
          name: 'Amanda7',
          firstmessage: 'Hello,It me',
          numberOfUnreadMessages: 22,
          timepass: '4 minute ago',
          isRead: false,
        },
        {
          url: 'https://randomuser.me/api/portraits/women/30.jpg',
          name: 'Amanda',
          firstmessage: 'Hello,It me',
          numberOfUnreadMessages: 22,
          timepass: '5 minute ago',
          isRead: true,
        },
        {
          url: 'https://randomuser.me/api/portraits/women/29.jpg',
          name: 'Amanda',
          firstmessage: 'Hello,It me',
          numberOfUnreadMessages: 22,
          timepass: '4 minute ago',
          isRead: false,
        },
      ]);
  return (
    <View>
       <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          height: 60,
        }}
      >
        <View style={{flexDirection: 'row', padding: 5}}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons
              style={{padding: 5}}
              name='ios-arrow-back-sharp'
              size={25}
              color={'#06a88e'}
            ></Ionicons>
          </TouchableOpacity>

          <Text style={{fontSize: 20, fontWeight: '500', lineHeight: 35}}>
            Select Contact
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
        <TouchableOpacity>
            <Ionicons
              style={{padding: 10}}
              name="search"
              size={25}
              color={'#06a88e'}></Ionicons>
          </TouchableOpacity>
          <TouchableOpacity>
            <Ionicons
              style={{padding: 10}}
              name='ellipsis-horizontal-circle-sharp'
              size={25}
              color={'#06a88e'}
            ></Ionicons>
          </TouchableOpacity>
        </View>
      </View>
      <FlatList
          style={{
            backgroundColor: 'white',
            
          }}
          data={chatHistory}
          ItemSeparatorComponent={renderSeparator}
          renderItem={renderItem}></FlatList>
           <TouchableOpacity
          style={{
            backgroundColor: '#06a88e',
            width: 60,
            height: 60,
            alignItems: 'center',
            
           
            borderRadius: 30,
            position: 'absolute',
            top:630, right:20
          }}
          onPress={() => navigation.navigate('Favorite')}
        >
         
          <Ionicons
            style={{lineHeight: 60}}
            name='people'
            size={25}
            color={'white'}
          ></Ionicons>
        </TouchableOpacity>
    </View>
  )
}