import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import React, {useState, useEffect} from 'react'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height
const itemWidth = windowWidth - 60

export default function Status ({navigation}) {
  const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#CED0CE',
        }}
      />
    )
  }
  function ChatItem ({dataItem}) {
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
            marginTop: 10,
          }}
        >
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity  onPress={() => navigation.navigate('ProfileFriend', {dataItem})}>
              <Image
                style={{
                  width: 60,
                  height: 60,
                  resizeMode: 'cover',
                  borderRadius: 30,
                }}
                source={{
                  uri: dataItem.urlavar,
                }}
              ></Image>
            </TouchableOpacity>

            <View style={{flexDirection: 'column'}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('ProfileFriend', {dataItem})}
              >
                <Text
                  style={{
                    marginLeft: 20,
                    fontWeight: 'bold',
                    fontSize: 18,
                  }}
                >
                  {dataItem.name}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Ionicons
                    style={{marginLeft: 20, alignSelf: 'center'}}
                    name='time'
                    size={15}
                    color={'#CED0CE'}
                  ></Ionicons>
                  <Text
                    style={{
                      marginLeft: 5,

                      fontSize: 12,
                    }}
                  >
                    {dataItem.firstmessage}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 20,
              }}
            >
              <Ionicons
                style={{marginRight: 5}}
                name='eye'
                size={15}
                color={'#CED0CE'}
              ></Ionicons>
              <Text style={{color: '#CED0CE'}}>{dataItem.eye}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <FlatList
          style={{flexDirection: 'row'}}
          horizontal={true}
          keyExtractor={(item, index) => index.toString()}
          data={dataItem?.img1}
          renderItem={renderItemimg}
        ></FlatList>
      </View>
    )
  }
  const renderItemimg = ({item, index}) => {
    return <Imgstatus dataItem={item} />
  }
  function Imgstatus ({dataItem}) {
    return (
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 100, height: 100, marginRight: 10}}
          source={{
            uri: dataItem.urlimg1,
          }}
        ></Image>
        <Image
          style={{width: 100, height: 100, marginRight: 10}}
          source={{
            uri: dataItem.urlimg2,
          }}
        ></Image>
        <Image
          style={{width: 100, height: 100, marginRight: 10}}
          source={{
            uri: dataItem.urlimg3,
          }}
        ></Image>
        <Image
          style={{width: 100, height: 100, marginRight: 10}}
          source={{
            uri: dataItem.urlimg4,
          }}
        ></Image>
        <Image
          style={{width: 100, height: 100, marginRight: 10}}
          source={{
            uri: dataItem.urlimg5,
          }}
        ></Image>
      </View>
    )
  }
  const renderItem = ({item, index}) => {
    return <ChatItem dataItem={item} />
  }

  const [chatHistory, setChatHistory] = useState([
    {
      urlavar: 'https://randomuser.me/api/portraits/women/30.jpg',

      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      img2: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Nitu Desia',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 12,
      timepass: '03:00 PM',
      isCall: true,
      eye: '173',
    },
    {
      urlavar: 'https://randomuser.me/api/portraits/women/20.jpg',
      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=6',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=7',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=8',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=9',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=10',
        },
      ],
      img2: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Rohit',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 33,
      timepass: '01:45 PM',
      isCall: false,
      eye: '763',
    },
    {
      urlavar: 'https://randomuser.me/api/portraits/women/22.jpg',
      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=11',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=12',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=13',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=14',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=15',
        },
      ],
      img2: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Marcello Di Giovanni',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 14,
      timepass: '01:52 PM',
      isCall: true,
      eye: '973',
    },
    {
      urlavar: 'https://randomuser.me/api/portraits/women/24.jpg',
      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=16',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=17',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=18',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=19',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=20',
        },
      ],
      img2: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Amanda4',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 0,
      timepass: 'Yesterday',
      isCall: false,
      eye: '734',
    },
    {
      urlavar: 'https://randomuser.me/api/portraits/women/26.jpg',
      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=21',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=22',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=23',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=24',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=25',
        },
      ],
      img2: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Amanda5',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: 'Yesterday',
      isCall: true,
      eye: '173',
    },
    {
      urlavar: 'https://randomuser.me/api/portraits/women/26.jpg',
      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Amanda6',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: '2 minute ago',
      isCall: true,
      eye: '723',
    },
    {
      urlavar: 'https://randomuser.me/api/portraits/women/31.jpg',
      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Amanda7',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: '4 minute ago',
      isCall: false,
      eye: '373',
    },
    {
      urlavar: 'https://randomuser.me/api/portraits/women/30.jpg',
      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Amanda',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: '5 minute ago',
      isCall: true,
      eye: '13',
    },
    {
      urlavar: 'https://randomuser.me/api/portraits/women/29.jpg',
      img1: [
        {
          urlimg1: 'https://source.unsplash.com/random/200x200?sig=1',
          urlimg2: 'https://source.unsplash.com/random/200x200?sig=2',
          urlimg3: 'https://source.unsplash.com/random/200x200?sig=3',
          urlimg4: 'https://source.unsplash.com/random/200x200?sig=4',
          urlimg5: 'https://source.unsplash.com/random/200x200?sig=5',
        },
      ],
      name: 'Amanda',
      firstmessage: 'Today, 09:21 PM',
      numberOfUnreadMessages: 22,
      timepass: '4 minute ago',
      isCall: false,
      eye: '23',
    },
  ])

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          borderBottomWidth: 0.5,
          borderBottomColor: '#CED0CE',
        }}
      >
        <TouchableOpacity style={{marginBottom: 20}}>
          <Image
            style={{
              width: 70,
              height: 70,
              borderRadius: 35,
              borderWidth: 2.5,
              borderColor: '#06a88e',
              marginTop: 10,
              marginLeft: 20,
            }}
            source={{uri: 'https://randomuser.me/api/portraits/women/19.jpg'}}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={{flexDirection: 'column', marginLeft: 20}}>
            <Text style={{fontSize: 18, fontWeight: '800'}}> My Status</Text>
            <Text style={{color: 'grey'}}> Tap to add status update</Text>
          </View>
        </TouchableOpacity>
      </View>
      <TouchableOpacity>
        <Text
          style={{
            color: '#06a88e',
            fontSize: 16,
            marginBottom: 10,
            marginLeft: 20,
          }}
        >
          New Status
        </Text>
      </TouchableOpacity>

      <FlatList
        style={{
          backgroundColor: 'white',
          marginLeft: 20,
          height: windowHeight,
        }}
        data={chatHistory}
        ItemSeparatorComponent={renderSeparator}
        renderItem={renderItem}
      ></FlatList>
    </View>
  )
}
