import {
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import React, {useState} from 'react';
import InputCommon from '../Components/InputCommon';
import ButtonCommon from '../Components/ButtonCommon';
import LogoCommon from '../Components/LogoCommon';
import BackButton from '../Components/BackButton';
import {
  auth,
  createUserWithEmailAndPassword,
  firebaseDatabase,
  firebaseDatabaseRef,
  firebaseSet,
  sendEmailVerification,
} from '../firebase/firebase';
import Loading from './Loading';
export default function SignUp({navigation}) {
  const [email, setEmail] = useState('minhluu15012000@gmail.com');
  const [name, setName] = useState('Minh Luu');
  const [password, setPassword] = useState('123456');
  const [confirmPW, setConfirmPW] = useState('123456');
  const [loading, setLoading] = useState(false);

  const validateEmail = email => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      );
  };

  const handleSignUp = async () => {
    if (email !== '' && password !== '' && confirmPW !== '' && name !== '') {
      if (password.length < 6) {
        Alert.alert('Warning', 'Password must be at least 6 characters');
      } else {
        if (password === confirmPW) {
          if (validateEmail(email) !== null) {
            await setLoading(true);
            await createUserWithEmailAndPassword(auth, email, password)
              .then(_user => {
                const user = _user.user;
                console.log('name', name);
                sendEmailVerification(auth.currentUser)
                  .then(() => {
                    Alert.alert('Alert', 'Email verification sent!');
                  })
                  .catch(err => {
                    console.log(err);
                  });
                firebaseSet(
                  firebaseDatabaseRef(firebaseDatabase, `users/${user.uid}`),
                  {
                    email: user.email,
                    emailVerified: user.emailVerified,
                    accessToken: user.accessToken,
                    friends: 0,
                    userName: name,
                  },
                );
                setLoading(false);
                Alert.alert('Alert', 'Sign up successfully!');
                navigation.navigate('SignIn');
              })
              .catch(err => console.log(err));
          } else {
            Alert.alert('Warning', 'Invalid email!');
          }
        } else {
          Alert.alert(
            'Warning',
            'Password and Confirm Password are not the same!',
          );
        }
      }
    } else {
      Alert.alert('Warning', 'Please fill out the fields completely!');
    }
  };

  return (
    <ScrollView style={[styles.containerSI, {opacity: loading ? 0.5 : 1}]}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <BackButton />
      </TouchableOpacity>
      <LogoCommon />
      <View style={styles.inputContainerSI}>
        <InputCommon
          iconName={'person'}
          iconSize={20}
          iconColor={'black'}
          placeholder={'Email...'}
          onChangeText={txt => setEmail(txt)}
          value={email}
        />
        <InputCommon
          iconName={'person'}
          iconSize={20}
          iconColor={'black'}
          placeholder={'Your name...'}
          onChangeText={txt => setName(txt)}
          value={name}
        />
        <InputCommon
          iconName={'lock-closed'}
          iconSize={20}
          iconColor={'black'}
          placeholder={'Password...'}
          onChangeText={txt => setPassword(txt)}
          value={password}
          secureTextEntry={true}
        />
        <InputCommon
          iconName={'lock-closed'}
          iconSize={20}
          iconColor={'black'}
          placeholder={'Confirm Password...'}
          onChangeText={txt => setConfirmPW(txt)}
          value={confirmPW}
          secureTextEntry={true}
        />
        <TouchableOpacity onPress={() => handleSignUp()}>
          <ButtonCommon btnName={'Sign Up'} />
        </TouchableOpacity>
      </View>
      {loading && <Loading />}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  // SignUpScreen
  containerSI: {
    backgroundColor: 'white',
    flex: 1,
  },
  inputContainerSI: {
    alignItems: 'center',
  },
});
