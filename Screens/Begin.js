import {View, Text, StyleSheet, Image} from 'react-native';
import React, {useEffect} from 'react';
import {DIMENSIONS} from '../Styles/Dimensions';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Begin({navigation}) {
  const handleExistUser = async () => {
    const user = await AsyncStorage.getItem('loggedUser');
    if (user !== null) {
      navigation.navigate('Favorite');
    } else {
      navigation.navigate('SignIn');
    }
  };
  useEffect(() => {
    setTimeout(() => {
      handleExistUser();
    }, 1900);
  }, []);
  return (
    <View style={styles.containerWS}>
      <View style={styles.subContainerWS}>
        <Image
          style={styles.logoWS}
          source={require('../Images/WaitingScreenLogo.png')}
        />
        <View>
          <Text style={styles.titleWS}>WhatsApp</Text>
          <Text style={styles.subTitleWS}>Always Secure</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  // WaitingScreen:
  containerWS: {
    backgroundColor: '#2ad983',
    flex: 1,
    justifyContent: 'center',
  },
  subContainerWS: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoWS: {
    width: DIMENSIONS.screenWidth / 5,
    height: DIMENSIONS.screenWidth / 5,
    marginRight: DIMENSIONS.screenWidth / 20,
  },
  titleWS: {
    fontSize: 35,
    color: 'white',
    fontWeight: 'bold',
  },
  subTitleWS: {
    color: 'white',
    fontSize: 14,
    alignSelf: 'flex-end',
  },
});
