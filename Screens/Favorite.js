import {
  View,
  Text,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Image,
  TextInput,
  StyleSheet,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Chatmenu from './Chatmenu';
import Groups from './Groups';
import Call from './Call';
import Status from './Status';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-async-storage/async-storage';

const slideWidth = Dimensions.get('window').width;
const slideHeight = Dimensions.get('window').height;
export default function Favorite({navigation}) {
  const [ProfileAccount] = [{
    name: 'Jessica',
    
    url: 'https://randomuser.me/api/portraits/women/19.jpg',
    firstName:'Jain'
  }]
  const [isModalVisible, setModalVisible] = useState(false);
  const [menu, setMenu] = useState('Chat');
  const listMenu = [
    {
      option: 'Chat',
    },
    {
      option: 'Groups',
    },
    {
      option: 'Status',
    },
    {
      option: 'Call',
    },
  ];
  const setMenuOption = menu => {
    return setMenu(menu);
  };
  const btnActive = {
    color: '#06a88e',
  };
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          height: 80,
        }}>
        <View style={{flexDirection: 'row', padding: 5}}>
          <TouchableOpacity onPress={() => navigation.navigate('ProfileAccount', {ProfileAccount})}>
          <Image
            style={{
              width: 60,
              height: 60,
              resizeMode: 'cover',
              borderRadius: 30,
              marginRight: 15,
              marginStart: 10,
            }}
            source={{
              uri: ProfileAccount.url,
            }}></Image>
        
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('ProfileAccount', {ProfileAccount})}>
          <Text style={{fontSize: 16, fontWeight: '500', lineHeight: 40}}>
            Hi, {ProfileAccount.name}
          </Text>
          </TouchableOpacity>
          
        </View>
        <View style={{flexDirection: 'row'}}>
          <Modal isVisible={isModalVisible}>
            <View
              style={{
                right: 0,
                flex: 1,
                alignItems: 'flex-end',
              }}>
              <View
                style={{
                  backgroundColor: 'white',
                  width: 200,
                  marginLeft: 250,
                  borderRadius: 5,
                }}>
                <TouchableOpacity
                  onPress={async () => {
                    AsyncStorage.removeItem('loggedUser').then(() => {
                      navigation.navigate('SignIn');
                    });
                  }}>
                  <Text style={{padding: 10, fontSize: 16, fontWeight: '700'}}>
                    New Chat
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text style={{padding: 10, fontSize: 16, fontWeight: '700'}}>
                    New Group
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text style={{padding: 10, fontSize: 16, fontWeight: '700'}}>
                    New Broadcast
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text style={{padding: 10, fontSize: 16, fontWeight: '700'}}>
                    Whatsapp Web
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text style={{padding: 10, fontSize: 16, fontWeight: '700'}}>
                    Start Messages
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text style={{padding: 10, fontSize: 16, fontWeight: '700'}}>
                    Switch Account
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Setting')}>
                  <Text style={{padding: 10, fontSize: 16, fontWeight: '700'}}>
                    Setting
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => setModalVisible(!isModalVisible)}>
                  <Text style={{padding: 10, fontSize: 16, fontWeight: '700'}}>
                    Close
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Search');
            }}>
            <Ionicons
              style={{padding: 10}}
              name="search"
              size={25}
              color={'#06a88e'}></Ionicons>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setModalVisible(!isModalVisible)}>
            <Ionicons
              style={{padding: 10}}
              name="ellipsis-horizontal-circle-sharp"
              size={25}
              color={'#06a88e'}></Ionicons>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{}}>
        <View
          style={{
            backgroundColor: '#fff',
            width: slideWidth,
            // height: slideHeight - 80,
            // flex: 1
          }}>
          <View style={{flexDirection: 'row', padding: 10}}>
            <TouchableOpacity>
              <Ionicons
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: 'gray',
                  borderBottomWidth: 0.5,
                  paddingBottom: 20,
                }}
                name="camera"
                size={20}
                color={'#b8bbbf'}></Ionicons>
            </TouchableOpacity>

            {listMenu.map(e => {
              return (
                <TouchableOpacity
                  key={e.option}
                  style={[
                    styles.detail__btnTab,
                    menu === e.option && {
                      borderBottomWidth: 2,
                      borderBottomColor: '#06a88e',
                    },
                  ]}
                  onPress={() => setMenuOption(e.option)}>
                  <Text
                    style={[
                      styles.detail__textTab,
                      menu === e.option && btnActive,
                    ]}>
                    {e.option}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>

          <View style={{}}>
            {menu === 'Chat' ? (
              <Chatmenu navigation={navigation}></Chatmenu>
            ) : (
              <View></View>
            )}
            {menu === 'Groups' ? (
              <Groups navigation={navigation}></Groups>
            ) : (
              <View></View>
            )}
            {menu === 'Status' ? (
              <Status navigation={navigation}></Status>
            ) : (
              <View></View>
            )}
            {menu === 'Call' ? (
              <Call navigation={navigation}></Call>
            ) : (
              <View></View>
            )}
          </View>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  detail__textTab: {
    fontSize: 14,
    color: 'gray',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  detail__btnTab: {
    width: Dimensions.get('window').width / 4.5,
    flexDirection: 'row',
    borderBottomColor: 'gray',
    borderBottomWidth: 0.5,

    justifyContent: 'center',
  },
  textCloseModal: {
    padding: 5,
    backgroundColor: '#58ABF6',
    borderRadius: 5,
    width: 50,
    color: '#fff',
    textAlign: 'center',
  },
  sortItem: {
    alignItems: 'center',
    width: '100%',
    paddingVertical: 10,
    borderRadius: 10,
    backgroundColor: '#F2F2F2',
    marginBottom: 10,
  },
});
