import {initializeApp} from 'firebase/app';
import {
  getReactNativePersistence,
  initializeAuth,
} from 'firebase/auth/react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  getAuth,
  onAuthStateChanged,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  sendEmailVerification,
  GoogleAuthProvider,
  signInWithCredential,
  FacebookAuthProvider,
  sendPasswordResetEmail,
  signOut
} from 'firebase/auth';
import {
  getDatabase,
  ref as firebaseDatabaseRef,
  set as firebaseSet,
  onValue,
  off,
  get,
  child,
  push,
} from 'firebase/database';

const firebaseConfig = {
  apiKey: 'AIzaSyAAueEG5Tf8fr9QvAWA7qVC1838pncKYX4',
  authDomain: 'chatapprn-a15a9.firebaseapp.com',
  databaseURL: 'https://chatapprn-a15a9-default-rtdb.firebaseio.com',
  projectId: 'chatapprn-a15a9',
  storageBucket: 'chatapprn-a15a9.appspot.com',
  appId: '1:939340392096:android:259bfaea3dbc2e60f20bc5',
  messagingSenderId: '939340392096',
};

const app = initializeApp(firebaseConfig);
const auth = initializeAuth(app, {
  persistence: getReactNativePersistence(AsyncStorage),
});
const firebaseDatabase = getDatabase(app);

export {
  auth,
  firebaseDatabase,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  firebaseDatabaseRef,
  firebaseSet,
  sendEmailVerification,
  onValue,
  onAuthStateChanged,
  
  signInWithCredential,
  
  sendPasswordResetEmail,
  signOut,
  get,
  child,
  push,

  
};
