import { View, Text } from 'react-native'
import React from 'react'
import MainNavigation from './Navigation/MainNavigation'

export default function App() {
  return (
    <MainNavigation />
  )
}
