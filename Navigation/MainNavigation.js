import {View, Text} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SignIn from '../Screens/SignIn';
import Home from '../Screens/Home';
import SignUp from '../Screens/SignUp';
import Begin from '../Screens/Begin';
import Favorite from '../Screens/Favorite';
import Message from '../Screens/Message';
import Contact from '../Screens/Contact';
import ProfileCall from '../Screens/ProfileCall';
import Search from '../Screens/Search';
import ProfileAccount from '../Screens/ProfileAccount';
import Setting from '../Screens/Setting';
import CallWait from '../Screens/CallWait';
import ProfileFriend from '../Screens/ProfileFriend';
import ForgotPW from '../Screens/ForgotPW';

const Stack = createNativeStackNavigator();

export default function MainNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Begin">
        <Stack.Screen
          name="Begin"
          component={Begin}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignIn"
          component={SignIn}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ForgotPW"
          component={ForgotPW}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={Favorite}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Favorite"
          component={Favorite}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Message"
          component={Message}
          options={{headerShown: false}}
        />
                <Stack.Screen
          name="ProfileCall"
          component={ProfileCall}
          options={{headerShown: false}}
        />
                <Stack.Screen
          name="Search"
          component={Search}
          options={{headerShown: false}}
        />
                <Stack.Screen
          name="Contact"
          component={Contact}
          options={{headerShown: false}}
        />
                <Stack.Screen
          name="ProfileAccount"
          component={ProfileAccount}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="Setting"
          component={Setting}
          options={{headerShown: false}}
        /> 
        <Stack.Screen
        name="CallWait"
        component={CallWait}
        options={{headerShown: false}}
      />
        <Stack.Screen
        name="ProfileFriend"
        component={ProfileFriend}
        options={{headerShown: false}}
      />
    
      </Stack.Navigator>
     
    
      
    </NavigationContainer>
  );
}
